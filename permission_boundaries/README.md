## Table of Contents

- [Introduction](#introduction)
- [Instructions (Click here to skip the explanations)](#instructions)
- [Notes](#notes)

## Introduction

$\textcolor{orange}{\textsf{This lab will guide you through the implementation of permissions boundaries on a terraform AWS IAM User.}}$  Utilizing Terraform to deploy your resources in AWS makes development, testing, and validation of design more efficient.  However, oftentimes developers / cloud engineers will implement over-privileged policies to their provisioned terraform users which creates risk in the event that the Terraform User's Access and Secret keys get stolen.

In this lab we're going to provision a terraform user, provide a permissions boundary to reduce the chances of privilege escalation, and provision an SCP (Service Control Policy) to prevent the detachment of the permissions boundary.  Additionally, with the permissions boundary we also will set a guardrail in place that prevents multiple user creation from the users the terraform_user creates AND enforces permissions boundaries on users created with the terraform_user.  After testing this lab, you should experiment with what permissions you want to limit within your AWS environment.

$\textcolor{red}{\textsf{Please do not simply deploy this code into your environment with no editing / configuration changes.}}$
$\textcolor{red}{\textsf{I take no responsibility for you deploying insecure resources and losing data.}}$

### Pre-requsites
1. Have an AWS account in which you have access to create resources and manage IAM
2. Have terraform installed on your local system [Terraform install docs](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli)
3. Basic understanding of Terraform (what it is, how to run commands)
4. AWS Organizations setup in your account (this requires more than one account)
5. Familiarity with the AWS CLI

### Instructions
To begin we're going to deploy a terraform AWS IAM user and provision it with an AWS Access and Secret key.  We'll then deploy a simple EC2 instance, a secret in Secrets Manager, and then further down in this lab explain a common issue with allowing user creation with Terraform.

$\textcolor{orange}{\textsf{Let's first quickly understand what a permissions boundary is:}}$
An AWS permissions boundary is like a safety net for IAM users or roles, ensuring they can only do what's allowed by both their own policies and the boundary's rules. It's a way to set a maximum limit on what actions they can take, even if their policies might allow more. This helps keep your AWS resources secure by preventing users from doing anything beyond what's explicitly allowed by the boundary, no matter what permissions they might have been given elsewhere.

To continue your learning with permissions boundaries you can reference this [AWS Documentation](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_boundaries.html)

#### Step 1: Deploy a terraform AWS IAM User
First go into the IAM console and navigate to "users"

![Users](Pictures/users.png)

Then click on "Create User"

![CreateUser](Pictures/createuser.png)

Give your terraform user a name you'll remember or can reference at a later time

![Username](Pictures/username.png)

Select the "Attach policies directly", then in this lab we're going to select the "AdministratorAccess" policy.  $\textcolor{red}{\textsf{DO NOT ATTACH THIS POLICY IN ANY ENVIRONMENT.}}$  This is merely a lab in a sandbox environment but the AdministratorAccess policy is never recommended for any environment.  Click the "next" button at the bottom of the screen.

![Policy](Pictures/policy.png)

Once you've confirmed your details, then create the user.

![Create](Pictures/create.png)

#### Step 2: Create your access keys for the terraform user
From the screen you've created the terraform_user click into the hyperlink on the name to access the user settings.

![Click](Pictures/click.png)

Click on Security Credentials under the user details

![Keys](Pictures/seccred.png)

Click on "Create Access Key"

![Access](Pictures/access.png)

As you can now experience (through the various messages of AWS telling you keys aren't the best way), access keys are not the best way to authenticate into AWS but in this case we're going to utilize access keys to make this lab easier.  A more recommended approach would be to utilize an assume role operation.  If you'd like to understand better ways to authenticate into AWS, please read the terraform docs [here](https://registry.terraform.io/providers/tfproviders/aws/latest/docs).  $\textcolor{red}{\textsf{Always remember to get rid of test access keys like these when you're finished with this lab!}}$

Scroll down and click on "Other" and then click "Next"

![Other](Pictures/other.png)

Add a tag so you know to delete this access key later, then click "Create Access Key"

![Tag](Pictures/tag.png)

Copy your access key and add it into your terraform configurations.  $\textcolor{red}{\textsf{Again, this is just a lab so please do NOT ever commit secrets to code!}}$  NOTE: in this lab there are two provider files, one in each folder within the terraform folder.

```hcl
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
  access_key = "youraccesskeyhere"
  secret_key = "yoursecretkeyhere"
}
```

#### Step 3: Deploy an example EC2 instance and secret
To deploy the terraform ec2 and secrets manager example navigate into the terraform -> Compute folder and run the following commands.  This ec2 instance is essentially useless since it's put in a private subnet with no outbound rules so don't attempt to access it.

```hcl
terraform init
terraform fmt
terraform validate
terraform apply
```

This example you'll have to use your imagination on but let's attempt to set the scene.  Let's say there is a secret that is stored in our AWS account that the terraform user shouldn't have access to.  In our case we deployed the secret WITH the terraform user, but in this fake scenario, the terraform user shouldn't have access to the secret.  With the administrator access policy, the terraform user can not only read the secret but delete it as well.  We'll fix this with our permission boundary later on in this lab!

For an example of how the terraform user can be utilized to read the secret in a way it shouldn't be, simply utilize the credentials (access key, secret key) in the AWS CLI and read the secret.

```
aws secretsmanager get-secret-value --secret-id YourSecretIdHere
```

#### Step 4: Deploy IAM policies needed to demonstrate this lab
Let's deploy the two policies that we'll need to use in this lab.  The two policies are the terraform_user_boundary as a permissions boundary as well as the terraform_company_boundary which is the permissions boundary that would be attached to any user the terraform_user creates.

NOTE: Ensure you change the account numbers or names in the policies to make this lab work properly!!!  Navigate to the terraform -> IAM folder.

```hcl
terraform init
terraform fmt
terraform validate
terraform apply
```

The terraform_user_boundary policy is explained below but the terraform_company_boundaries policy is a boundary that you want to be assigned to all users, even the users that the terraform user provisions.  One thing to note is the company boundaries prevent the user that the terraform_user provisions from creating a user.  This is a good control because if an attacker were able to generate access keys on the user that the terraform_user provisions, they could create many users and access keys and run different attacks from each user.  The attacker would likely masquerade as legitimate users but also create noise with other users so that incident response folks would have to dig into many different users or find a pattern to identify the attacker's activities.

If you're curious and would like to see how this attack or defense can be automated, please contact me and I'll generate another lab with that content :)

#### Step 5: Configure a permission boundary on the terraform user
Utilizing your non-terraform user (AKA the user you used to create the terraform_user), we're going to assign a permissions boundary onto the terraform_user.  In a production environment you'll likely have this process automated and an SCP implemented via controltower customizations.

The permissions boundary we have below does many things.  Let's walk down the policy by each section.

1. The first section allows the terraform_user to create users, roles, etc ONLY if they have a permissions boundary attached.  In this case it's the "terraform_company_boundaries".
2. The second section denies attaching any policy that has "FullAccess" in its name
3. The third section of our policy prevents attaching the AdministratorAccess policy
4. The fourth section of our policy allows the terraform user to perform all actions besides ones that we define they can't.  This needs to be widdled down more and ideally you'd also have SCPs preventing certain actions as well. 
5. The fifth section prevents the terraform user from editing the permissions boundary for itself OR the terraform company boundaries
6. The sixth section prevents simply deleting the permissions boundaries
7. The seventh section is needed to allow the terraform user to perform IAM actions but DISALLOWING it to touch the automation users or users that provision the permission boundary of the terraform_user
8. The eighth section is where we deny anything dealing with secrets manager, which protects our secret stored that the terraform_user shouldn't be able to access

For steps on how to implement, scroll to right below the policy.

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "CreateChangeOnlyWithBoundary",
            "Effect": "Allow",
            "Action": [
                "iam:AttachUserPolicy",
                "iam:AttachRolePolicy",
                "iam:CreateUser",
                "iam:CreateRole",
                "iam:DeleteUserPolicy",
                "iam:DetachUserPolicy",
                "iam:DeleteRolePolicy",
                "iam:DetachRolePolicy",
                "iam:PutUserPermissionsBoundary",
                "iam:PutRolePermissionsBoundary",
                "iam:PutUserPolicy",
                "iam:PutRolePolicy"
            ],
            "Resource": "*",
            "Condition": {
                "StringEquals": {
                    "iam:PermissionsBoundary": "arn:aws:iam::youraccountnumberhere:policy/terraform_company_boundaries"
                }
            }
        },
        {
            "Effect": "Deny",
            "Action": [
                "iam:AttachUserPolicy",
                "iam:AttachGroupPolicy",
                "iam:AttachRolePolicy"
            ],
            "Resource": "*",
            "Condition": {
                "ForAnyValue:ArnLike": {
                    "iam:PolicyARN": "arn:aws:iam::aws:policy/*FullAccess*"
                }
            }
        },
                {
            "Sid": "DenyAttachingAdminPolicyExtraPrecaution",
            "Effect": "Deny",
			"Action": "iam:AttachUserPolicy",
            "Condition": {
                "ArnEqualsIfExists": {
                    "iam:PolicyARN": "arn:aws:iam::aws:policy/AdministratorAccess"
                }
            },
			"Resource": "*"
        },
        {
            "Sid": "TerraformAllowAll",
            "Effect": "Allow",
            "NotAction": [
                "iam:*",
                "organizations:*",
                "account:*"
            ],
            "Resource": "*"
        },
        {
            "Sid": "NoBoundaryPolicyEdit",
            "Effect": "Deny",
            "Action": [
                "iam:CreatePolicyVersion",
                "iam:DeletePolicy",
                "iam:DeletePolicyVersion",
                "iam:SetDefaultPolicyVersion"
            ],
            "Resource": [
                "arn:aws:iam::youraccountnumberhere:policy/terraform_company_boundaries",
                "arn:aws:iam::youraccountnumberhere:policy/terraform_user_boundary"
            ]
        },
        {
            "Sid": "NoBoundaryUserDelete",
            "Effect": "Deny",
            "Action": [
                "iam:DeleteUserPermissionsBoundary",
                "iam:DeleteRolePermissionsBoundary"],
            "Resource": "*"
        },
        {
            "Sid": "IAMTasks",
            "Effect": "Allow",
            "Action": [
              "iam:CreateGroup",
              "iam:CreatePolicy",
              "iam:DeleteGroup",
              "iam:DeletePolicy",
              "iam:DeletePolicyVersion",
              "iam:DeleteUser",
              "iam:GetGroup",
              "iam:GetGroupPolicy",
              "iam:GetInstanceProfile",
              "iam:GetPolicy",
              "iam:GetPolicyVersion",
              "iam:GetRole",
              "iam:GetRolePolicy",
              "iam:GetSAMLProvider",
              "iam:GetServiceLastAccessedDetails",
              "iam:GetServiceLastAccessedDetailsWithEntities",
              "iam:GetServiceLinkedRoleDeletionStatus",
              "iam:GetUser",
              "iam:GetUserPolicy",
              "iam:GetAccessKeyLastUsed",
              "iam:GetAccountAuthorizationDetails",
              "iam:GetAccountEmailAddress",
              "iam:GetAccountName",
              "iam:GetCloudFrontPublicKey",
              "iam:GetContextKeysForCustomPolicy",
              "iam:GetContextKeysForPrincipalPolicy",
              "iam:List*",
              "iam:SetDefaultPolicyVersion",
              "iam:SimulateCustomPolicy",
              "iam:SimulatePrincipalPolicy",
              "iam:UpdateGroup",
              "iam:UpdateUser",
              "iam:AddClientIDToOpenIDConnectProvider",
              "iam:AddRoleToInstanceProfile",
              "iam:AddUserToGroup",
              "iam:CreateAccountAlias",
              "iam:CreateInstanceProfile",
              "iam:CreateOpenIDConnectProvider",
              "iam:CreateServiceLinkedRole",
              "iam:CreateServiceSpecificCredential",
              "iam:DeleteAccountAlias",
              "iam:DeleteCloudFrontPublicKey",
              "iam:DeleteInstanceProfile",
              "iam:DeleteOpenIDConnectProvider",
              "iam:DeleteRole",
              "iam:DeleteServiceLinkedRole",
              "iam:DeleteServiceSpecificCredential",
              "iam:PassRole",
              "iam:RemoveClientIDFromOpenIDConnectProvider",
              "iam:RemoveRoleFromInstanceProfile",
              "iam:RemoveUserFromGroup",
              "iam:ResetServiceSpecificCredential",
              "iam:SetSecurityTokenServicePreferences",
              "iam:SetSTSRegionalEndpointStatus",
              "iam:UpdateCloudFrontPublicKey",
              "iam:UpdateGroup",
              "iam:UpdateOpenIDConnectProviderThumbprint",
              "iam:UpdateRole",
              "iam:UpdateRoleDescription",
              "iam:UpdateServiceSpecificCredential",
              "iam:UpdateSigningCertificate",
              "iam:UpdateUser",
              "iam:UploadCloudFrontPublicKey",
              "iam:TagInstanceProfile",
              "iam:TagOpenIDConnectProvider",
              "iam:TagPolicy",
              "iam:TagRole",
              "iam:TagUser",
              "iam:UntagInstanceProfile",
              "iam:UntagOpenIDConnectProvider",
              "iam:UntagPolicy",
              "iam:UntagRole",
              "iam:UntagUser"
            ],
            "NotResource": "arn:aws:iam::youraccountnumberhere:user/usersyouneedtoprotect"
        },
        {
            "Effect": "Deny",
            "Action": "secretsmanager:*",
            "Resource": "*"
        }
    ]
}
```
To implement this permissions boundary manually we'll need to go into the IAM console and navigate to "Users".  NOTE: you'll want to make sure you change any account numbers in the policies!!!

![Users](Pictures/users.png)

Then navigate to the terraform_user

![Click](Pictures/click.png)

Now we're going to scroll down and click on the "Permissions boundary" drop down

![Perm](Pictures/perm.png)

Next we're going to select the permissions boundary

![Select](Pictures/select.png)

Now we're going to search up our user boundary for terraform, select it, and then click "Set boundary"

![Set](Pictures/set.png)

It should push you back to your user screen where you can confirm that the permission boundary is now in effect

![Confirm](Pictures/confirm.png)

This can also be completed with this command if you're wanting to quicken the process or automate it.

```
aws iam put-user-permissions-boundary --permissions-boundary arn:aws:iam::youraccountnumberhere:policy/terraform_user_boundary --user-name terraform_user
```


#### Step 6: Attempt different bypasses and view commands to prove permissions boundary is in effect
We'll be testing the following bypasses / view commands:
* Viewing the secret in Secrets Manager
* Deleting the permissions boundary for the terraform_user
* Attempting to create a user without permissions boundaries with the terraform_user
* Attempt to create a user WITH permissions boundaries with the terraform_user

Once we have a permissions boundary in effect for the terraform_user we can test it by doing two different things.  The first thing we're going to attempt is to view the secret in secrets manager that we could view before.  To do this run the AWS CLI command to test viewing the secret.  You SHOULD be denied access.

```
aws secretsmanager get-secret-value --secret-id TestSecret
```
![Deny](Pictures/deny.png)

Next, we can attempt to remove the permissions boundary that is in effect for the terraform_user by running the below command.  You SHOULD be denied here as well.

```
aws iam delete-user-permissions-boundary --user-name terraform_user
```

![Denyp](Pictures/denyp.png)

After that let's attempt to create a user without permissions boundaries with our terraform_user.  This should fail as well.

```
aws iam create-user --user-name Bob
```

![Denyu](Pictures/denyu.png)

Now let's attempt to create a user with a permissions boundary in place.  This should succeed!

```
aws iam create-user --user-name Bob --permissions-boundary arn:aws:iam::youraccountnumberhere:policy/terraform_company_boundaries
```

![Success](Pictures/success.png)
![Success1](Pictures/success1.png)

To checkout all of the test cases that I ran, please see the test_cases.txt file.  If you see anything that does not look correct with the test cases, please let me know and I can adjust the lab!


#### Step 7: Configure the SCP to prevent detaching the policy
If you're in a production environment you're likely going to have an AWS IAM user that provisions the terraform users and this SCP will help protect the provisioning user.  We can set a strict guardrail that no user / role / etc can remove a permissions boundary unless performed by ControlTower.

The Service Control Policy below does the following things::
- Prevents any user but ControlTower from deleting permissions boundaries
- Prevents any user but ControlTower from editing permissions boundaries
- Protect Service User provisioning Permissions Boundary from being deleted

```json
{
    "Version": "2012-10-17",
    "Statement": [
       {
         "Effect": "Deny",
         "Action": "iam:DeleteUserPermissionsBoundary",
         "Resource": "*",
         "Condition": {
           "ArnNotLike": {
             "aws:PrincipalARN": [
               "arn:aws:iam::youraccountnumberhere:user/controlTower"
             ]
           }
         }
       },
       {
           "Sid": "NoBoundaryPolicyEdit",
           "Effect": "Deny",
           "Action": [
               "iam:CreatePolicyVersion",
               "iam:DeletePolicy",
               "iam:DeletePolicyVersion",
               "iam:SetDefaultPolicyVersion"
           ],
           "Resource": [
               "arn:aws:iam::youraccountnumberhere:policy/terraform_company_boundaries",
               "arn:aws:iam::youraccountnumberhere:policy/terraform_user_boundary"
           ],
           "Condition": {
            "ArnNotLike": {
              "aws:PrincipalARN": [
                "arn:aws:iam::youraccountnumberhere:user/controlTower"
              ]
            }
          }
       }
    ]
   }
```

If you're not very familiar with SCPs, I recommend doing some reading on what they are and how they overlap with IAM policies and other policies/guardrails within AWS.  However, I will go through and show you how to implement an SCP if you have already completed getting your AWS organizations setup and have a secondary account to apply this control onto.

Navigate to the "AWS Organizations" service and click into the OU where you want to apply this SCP

![Org](Pictures/orgs.png)

Navigate to the "Policies" tab

![Policies](Pictures/policies.png)

Scroll down to the "Service Control Policies" section and click on "Attach"

![Attach](Pictures/attach.png)

From here we can click on the "Create policy" button

![CreatePol](Pictures/createpol.png)

Give your SCP a recognizeable name and description and then scroll down and paste in the SCP code from this lab.

![SCP](Pictures/scp.png)

![SCPCreate](Pictures/scpcreate.png)

Now that we've created it, we'll attach it

![SCPAttach](Pictures/scpattach.png)

Your policy is now attached!  If you're curious you can run this command to ensure the deletion portion of the SCP is functioning correctly.  This command should fail!  Note: To truly test this you'll need to run this command from another user that isn't the terraform_user.

```
aws iam delete-user-permissions-boundary --user-name terraform_user
```

![Denied](Pictures/denied.png)

#### Step 8: Tear down any resources and delete access keys
Remember to always tear down any resources at the end of your lab and delete any of the access keys that you've provisioned.  This is an important practice to remember as long-lived access keys are a treasure trove for attackers!

Go into your AWS IAM User for the terraform_user and delete the access key under the "Security Credentials" tab

```hcl
terraform destroy
```