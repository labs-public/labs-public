# Generate a random password for the secret
resource "random_password" "password" {
  length           = 16
  special          = true
  override_special = "_%@"
}

# Create a secret in AWS Secrets Manager
resource "aws_secretsmanager_secret" "example_secret" {
  name        = "example_secret_name"
  description = "This is an example secret"
}

# Create a version of the secret with the generated password
resource "aws_secretsmanager_secret_version" "example_secret_version" {
  secret_id = aws_secretsmanager_secret.example_secret.id
  secret_string = jsonencode({
    username = "example_user"
    password = random_password.password.result
  })
}