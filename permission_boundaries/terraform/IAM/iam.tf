resource "aws_iam_policy" "tf_cmp_boundary" {
  name        = "terraform_company_boundaries"
  path        = "/"
  description = "Terraform Company Boundary"

  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Sid" : "AllowAll",
        "Effect" : "Allow",
        "NotAction" : [
          "organizations:*",
          "account:*"
        ],
        "Resource" : "*"
      },
      {
        "Sid" : "DenyAllIAM",
        "Effect" : "Deny",
        "Action" : [
          "iam:*"
        ],
        "Resource" : "*"
      },
      {
        "Sid" : "DenyAttachingAdminPolicyExtraPrecaution",
        "Effect" : "Deny",
        "Action" : "iam:AttachUserPolicy",
        "Condition" : {
          "ArnEqualsIfExists" : {
            "iam:PolicyARN" : "arn:aws:iam::aws:policy/AdministratorAccess"
          }
        },
        "Resource" : "*"
      }
    ]
  })
}

resource "aws_iam_policy" "tf_usr_boundary" {
  name        = "terraform_user_boundary"
  path        = "/"
  description = "Terraform User Boundary"

  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Sid" : "CreateChangeOnlyWithBoundary",
        "Effect" : "Allow",
        "Action" : [
          "iam:AttachUserPolicy",
          "iam:AttachRolePolicy",
          "iam:CreateUser",
          "iam:CreateRole",
          "iam:DeleteUserPolicy",
          "iam:DetachUserPolicy",
          "iam:DeleteRolePolicy",
          "iam:DetachRolePolicy",
          "iam:PutUserPermissionsBoundary",
          "iam:PutRolePermissionsBoundary",
          "iam:PutUserPolicy",
          "iam:PutRolePolicy"
        ],
        "Resource" : "*",
        "Condition" : {
          "StringEquals" : {
            "iam:PermissionsBoundary" : "arn:aws:iam::youraccountnumberhere:policy/terraform_company_boundaries"
          }
        }
      },
      {
        "Effect" : "Deny",
        "Action" : [
          "iam:AttachUserPolicy",
          "iam:AttachGroupPolicy",
          "iam:AttachRolePolicy"
        ],
        "Resource" : "*",
        "Condition" : {
          "ForAnyValue:ArnLike" : {
            "iam:PolicyARN" : "arn:aws:iam::aws:policy/*FullAccess*"
          }
        }
      },
      {
        "Sid" : "DenyAttachingAdminPolicyExtraPrecaution",
        "Effect" : "Deny",
        "Action" : "iam:AttachUserPolicy",
        "Condition" : {
          "ArnEqualsIfExists" : {
            "iam:PolicyARN" : "arn:aws:iam::aws:policy/AdministratorAccess"
          }
        },
        "Resource" : "*"
      },
      {
        "Sid" : "TerraformAllowAll",
        "Effect" : "Allow",
        "NotAction" : [
          "iam:*",
          "organizations:*",
          "account:*"
        ],
        "Resource" : "*"
      },
      {
        "Sid" : "NoBoundaryPolicyEdit",
        "Effect" : "Deny",
        "Action" : [
          "iam:CreatePolicyVersion",
          "iam:DeletePolicy",
          "iam:DeletePolicyVersion",
          "iam:SetDefaultPolicyVersion"
        ],
        "Resource" : [
          "arn:aws:iam::youraccountnumberhere:policy/terraform_company_boundaries",
          "arn:aws:iam::youraccountnumberhere:policy/terraform_user_boundary"
        ]
      },
      {
        "Sid" : "NoBoundaryUserDelete",
        "Effect" : "Deny",
        "Action" : [
          "iam:DeleteUserPermissionsBoundary",
        "iam:DeleteRolePermissionsBoundary"],
        "Resource" : "*"
      },
      {
        "Sid" : "IAMTasks",
        "Effect" : "Allow",
        "Action" : [
          "iam:CreateGroup",
          "iam:CreatePolicy",
          "iam:DeleteGroup",
          "iam:DeletePolicy",
          "iam:DeletePolicyVersion",
          "iam:DeleteUser",
          "iam:GetGroup",
          "iam:GetGroupPolicy",
          "iam:GetInstanceProfile",
          "iam:GetPolicy",
          "iam:GetPolicyVersion",
          "iam:GetRole",
          "iam:GetRolePolicy",
          "iam:GetSAMLProvider",
          "iam:GetServiceLastAccessedDetails",
          "iam:GetServiceLastAccessedDetailsWithEntities",
          "iam:GetServiceLinkedRoleDeletionStatus",
          "iam:GetUser",
          "iam:GetUserPolicy",
          "iam:GetAccessKeyLastUsed",
          "iam:GetAccountAuthorizationDetails",
          "iam:GetAccountEmailAddress",
          "iam:GetAccountName",
          "iam:GetCloudFrontPublicKey",
          "iam:GetContextKeysForCustomPolicy",
          "iam:GetContextKeysForPrincipalPolicy",
          "iam:List*",
          "iam:SetDefaultPolicyVersion",
          "iam:SimulateCustomPolicy",
          "iam:SimulatePrincipalPolicy",
          "iam:UpdateGroup",
          "iam:UpdateUser",
          "iam:AddClientIDToOpenIDConnectProvider",
          "iam:AddRoleToInstanceProfile",
          "iam:AddUserToGroup",
          "iam:CreateAccountAlias",
          "iam:CreateInstanceProfile",
          "iam:CreateOpenIDConnectProvider",
          "iam:CreateServiceLinkedRole",
          "iam:CreateServiceSpecificCredential",
          "iam:DeleteAccountAlias",
          "iam:DeleteCloudFrontPublicKey",
          "iam:DeleteInstanceProfile",
          "iam:DeleteOpenIDConnectProvider",
          "iam:DeleteRole",
          "iam:DeleteServiceLinkedRole",
          "iam:DeleteServiceSpecificCredential",
          "iam:PassRole",
          "iam:RemoveClientIDFromOpenIDConnectProvider",
          "iam:RemoveRoleFromInstanceProfile",
          "iam:RemoveUserFromGroup",
          "iam:ResetServiceSpecificCredential",
          "iam:SetSecurityTokenServicePreferences",
          "iam:SetSTSRegionalEndpointStatus",
          "iam:UpdateCloudFrontPublicKey",
          "iam:UpdateGroup",
          "iam:UpdateOpenIDConnectProviderThumbprint",
          "iam:UpdateRole",
          "iam:UpdateRoleDescription",
          "iam:UpdateServiceSpecificCredential",
          "iam:UpdateSigningCertificate",
          "iam:UpdateUser",
          "iam:UploadCloudFrontPublicKey",
          "iam:TagInstanceProfile",
          "iam:TagOpenIDConnectProvider",
          "iam:TagPolicy",
          "iam:TagRole",
          "iam:TagUser",
          "iam:UntagInstanceProfile",
          "iam:UntagOpenIDConnectProvider",
          "iam:UntagPolicy",
          "iam:UntagRole",
          "iam:UntagUser"
        ],
        "NotResource" : "arn:aws:iam::youraccountnumberhere:user/usersyouneedtoprotect"
      },
      {
        "Effect" : "Deny",
        "Action" : "secretsmanager:*",
        "Resource" : "*"
      }
    ]
  })
}