terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

# Configure the AWS Provider
# DO NOT COMMIT THIS
provider "aws" {
  region     = "us-east-1"
  access_key = "youraccesskey"
  secret_key = "yoursecretkey"
}