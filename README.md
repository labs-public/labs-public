## Home of my gitlab labs where I show solutions to common cloudsecurity and cybersecurity problems
New labs will be published as I can push them out

## Labs currently available
* [Lambda Functions and Secrets Manager](https://gitlab.com/cprec/Github-Public-Imported-Labs/-/tree/main/AWS/Lambda%20Functions%20and%20Secrets%20Manager?ref_type=heads)
* [Navigating to zero trust with Storage Accounts](https://gitlab.com/cprec/Github-Public-Imported-Labs/-/tree/main/Azure/Navigating%20to%20zero%20trust%20with%20Storage%20Accounts?ref_type=heads)
* [Utilizing the ssh_agent_passphrase](https://gitlab.com/cprec/Github-Public-Imported-Labs/-/tree/main/MISC/ssh_agent_passphrase?ref_type=heads)
* [Preventing unauthorized users from accessing the Azure AD Portal](https://gitlab.com/cprec/Github-Public-Imported-Labs/-/tree/main/Azure/Preventing%20unauthorized%20users%20from%20accessing%20Azure%20AD%20Portal?ref_type=heads)
* [ADCS Certified PreOwned](https://gitlab.com/cprec/Github-Public-Imported-Labs/-/tree/main/MISC/ADCS_Certified_PreOwned?ref_type=heads)

## More labs to come
Please request a lab if you'd like to see a concept labbed out